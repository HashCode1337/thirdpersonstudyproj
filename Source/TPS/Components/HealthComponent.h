// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "Components/ActorComponent.h"

#include "TPS/FuncLibrary/Types.h"

#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnDamageChanged, AActor*, Instigator, float, Damage, AActor*, Victim, FHashCodeDamageDetails&, DamageDetails);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnKilled, AActor*, Instigator, AActor*, Victim, FHashCodeDamageDetails&, DamageDetails);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class TPS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	//Delegates
	FOnDamageChanged OnDamageChanged;
	FOnKilled OnKilled;

	float Health = 100;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float Armor = 1;
	float LastHitTime = 0;
	float LastHitDamage = 0;

	bool bIsAlive = true;
	bool bIgnoreDamage = false;

	AActor* ComponentOwner = nullptr;
	AActor* LashHitInstigator = nullptr;

public:

	//Setters and adders
	void SetHealth(float HealthVal, AActor* Instigator);
	void AddDamage(float Damage, FHashCodeDamageDetails& DamageDetails);
	void AllowDamage(bool bIgnoreDamageDealing);
	void SetArmor(float NewArmor);
	void AddArmor(float ArmorToAdd);

	//Getters
	UFUNCTION(BlueprintCallable)
	float GetDamage();
	UFUNCTION(BlueprintCallable)
	float GetHealth();
	UFUNCTION(BlueprintCallable)
	float GetHealthRanged();
	UFUNCTION(BlueprintCallable)
	float GetArmor();
	UFUNCTION(BlueprintCallable)
	bool GetIsAlive();
	UFUNCTION(BlueprintCallable)
	bool GetIsInvulnerable();

	//Events
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Health")
	void OnDeath(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails);
	void OnDeath_Implementation(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Health")
	void OnGetDamage(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails);
	void OnGetDamage_Implementation(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
