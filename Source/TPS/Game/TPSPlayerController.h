// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPSPlayerController.generated.h"

class ATPSCharacter;
class AItemBoxBase;

UCLASS(Blueprintable)
class ATPSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATPSPlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TPS | Visual Helpers")
	bool DebugMode = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TPS | Touch Sphere Radius")
	int32 ScanCapsuleRadius = 75;

	UPROPERTY()
	TArray<AActor*> InteractableActors;

	UFUNCTION(BlueprintCallable, Category = "TPS interaction")
	TArray<AActor*> GetInteractableActorsList();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;

	// End PlayerController interface

	AItemBoxBase* LastHighLightedActor = nullptr;
	float HighlightDistance = 100;


	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	bool DrawVisualTraceToCursorTarget(FHitResult& HitResult);

private:

	ATPSCharacter* MyCharacter = nullptr;
	FHitResult Res;

};


