// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LightBroken.generated.h"

class UPointLightComponent;

UCLASS()
class TPS_API ALightBroken : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALightBroken();

	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category = "LightSettings")
	float LuxMax = 8000;
	UPROPERTY(BlueprintReadWrite,EditAnywhere, Category = "LightSettings")
	float LuxMin = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LightSettings")
	float Frequency = 0.02f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LightSettings")
	bool bUseLightTemperature = false;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LightSettings")
	float LightTemperature = 1000.f;

	UPROPERTY()
	UPointLightComponent* LightComp;
	UPROPERTY()
	UPointLightComponent* PointLightComp;





protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
