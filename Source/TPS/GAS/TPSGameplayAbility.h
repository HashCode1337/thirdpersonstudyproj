// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/FuncLibrary/Types.h"
#include "Abilities/GameplayAbility.h"
#include "TPSGameplayAbility.generated.h"

/**
 * 
 */

UCLASS()
class TPS_API UTPSGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:

	UTPSGameplayAbility();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	ETPSInputID AbilityInputID = ETPSInputID::Key;

	UFUNCTION()
	ETPSInputID GetAbilityInputID();

};
