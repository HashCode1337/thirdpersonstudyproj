// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/PointLightComponent.h"

#include "Components/DecalComponent.h"
#include "Materials/MaterialInterface.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "TPS/FuncLibrary/Types.h"

#include "TPS/InterfaceHelper.h"

#include "Kismet/KismetMathLibrary.h"
#include "ItemBoxBase.generated.h"

class ATPSCharacter;
class UInventoryComponent;

UCLASS(Blueprintable)
class TPS_API AItemBoxBase : public AActor, public IInterfaceHelper
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemBoxBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType AmmoFor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AmmoCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* PlaneWithAmmoPreviewTexture = nullptr;

public:

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent* Root = nullptr;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BoxMesh = nullptr;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UPointLightComponent* LightSpotter = nullptr;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UDecalComponent* Decal = nullptr;

	float x = 0;
	float y = 0;
	float centrX = 0;

	float alpha = 1;
	float radius = 1;

	UMaterialInstanceDynamic* DynMat = nullptr;
	UMaterialInterface* DynamicMatInterface = nullptr;

	void SetDefaultDecalColor();
	void SetActiveDecalColor();
	void TogglePreviewMeshVisibility(bool bVisible);
	FVector GetAmmoBoxWorldLocation();

	EBoxColor CurrentDecalColor = EBoxColor::Default;
	EBoxColor GetCurrentEnumColor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual EInteractionType GetInteractionType() override;
	virtual bool ExecuteInteract(ATPSCharacter* ActionInstigator);

};
