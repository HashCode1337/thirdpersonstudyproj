// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Items/ProjectileBase.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionShape"));
	CollComp->SetSphereRadius(16.f);
	CollComp->SetCanEverAffectNavigation(false);
	CollComp->bReturnMaterialOnMove = true;

	RootComponent = CollComp;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletMesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletFX"));
	BulletFX->SetupAttachment(RootComponent);

	ProjMoveComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjMoveComp"));
	ProjMoveComp->UpdatedComponent = RootComponent;
	ProjMoveComp->InitialSpeed = 1.f;
	ProjMoveComp->MaxSpeed = 1000.f;

	ProjMoveComp->bRotationFollowsVelocity = true;
	ProjMoveComp->bShouldBounce = true;

}

void AProjectileBase::InitBullet(const FProjectileInfo& ProjIn)
{
	ProjSettings = ProjIn;

	SetLifeSpan(ProjSettings.ProjectileLifeTime);

}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	//CollComp->bReturnMaterialOnMove = true;
	ProjMoveComp->InitialSpeed = 1000.f;

	CollComp->OnComponentHit.AddDynamic(this,&AProjectileBase::OnCollHit);
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (BulletFX)
	{
		float BulletSpeed = ProjMoveComp->Velocity.Size();
		if (BulletSpeed < 5000.f)
		{
			BulletFX->DestroyComponent();
		}
	}
}

void AProjectileBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	UE_LOG(LogTemp, Warning, TEXT("Bullet hit %s"), *OtherActor->GetName());
}

void AProjectileBase::OnCollHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	

	if (OtherActor) 
	{		
		EPhysicalSurface PhysMat = UGameplayStatics::GetSurfaceType(Hit);

		bool ContainsDec = ProjSettings.HitDecals.Contains(PhysMat);

		if (ContainsDec)
		{
			UMaterialInterface* MyMat = ProjSettings.HitDecals[PhysMat];
			if (MyMat && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(MyMat, FVector(20.f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.f);
			}
		}

		bool ContainsPar = ProjSettings.HitParticles.Contains(PhysMat);

		if (ContainsPar)
		{
			if (OtherComp) 
			{
				UParticleSystem* ParSys = ProjSettings.HitParticles[PhysMat];
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParSys, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.f)));
			}
		}

		bool ContainsSounds = ProjSettings.HitSounds.Contains(PhysMat);

		if (ContainsSounds)
		{
			if (OtherComp)
			{
				USoundBase* ParSound = ProjSettings.HitSounds[PhysMat];
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ParSound, Hit.ImpactPoint);
			}
		}

		UGameplayStatics::ApplyDamage(OtherActor, ProjSettings.ProjectileDamage, GetInstigatorController(), this, NULL);

	}

}
