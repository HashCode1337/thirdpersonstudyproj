// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Game/TPSGameInstance.h"

bool UTPSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo &WeaponInfoResult)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponDataTable)
	{
		WeaponInfoRow = WeaponDataTable->FindRow<FWeaponInfo>(WeaponName, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			WeaponInfoResult = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
		UE_LOG(LogTemp, Warning, TEXT("Check!!! Maybe GameInstance not set correctly!"));
	}

	return bIsFind;
}