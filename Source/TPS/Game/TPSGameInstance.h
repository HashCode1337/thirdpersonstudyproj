// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS/Items/WeaponBase.h"
#include "TPSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()


public:

	//Prop
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	UDataTable* WeaponDataTable = nullptr;
	
public:

	//Func
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo &WeaponInfoResult);

};