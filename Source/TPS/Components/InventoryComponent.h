// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Game/TPSGameInstance.h"
#include "InventoryComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class TPS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()


public:	

	FString CompName = TEXT("KAVOTAVO");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FHashCodeWeaponsInventory Inventory;

	//Maybe we don't need it to be visible???
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	AActor* InventoryOwner;

	// Sets default values for this component's properties
	UInventoryComponent();

public:


	int32 SwitchNextWeapon();
	int32 SwitchPrevWeapon();
	void ClearUnregistredItems();

	bool SelectSlotAndApply(int32 SlotNumber);
	UFUNCTION(BlueprintCallable)
	bool AddWeaponToInventory(FName WeaponName, bool TakeInHands = false);
	UFUNCTION(BlueprintCallable)
	bool IsWeaponAlreadyExist(FName WeaponName);
	UFUNCTION(BlueprintCallable)
	bool DeleteWeaponFromInventory(int32 WeaponIndex);
	UFUNCTION(BlueprintCallable)
	bool HasFreeSpace();
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentSelectedSlot();
	UFUNCTION(BlueprintCallable)
	void SetCurrentSelectedSlot(int32 CurSlot);
	UFUNCTION(BlueprintCallable)
	bool IsAmmoTypeSlotAlreadyExits(EWeaponType WeaponAmmoType, int32& index);
	bool IsAmmoTypeSlotAlreadyExits(FName WeaponName, int32& index);
	UFUNCTION(BlueprintCallable)
	int32 FindAmmoSlotIndexByName(FName WeaponName);
	UFUNCTION(BlueprintCallable)
	int32 FindWeaponSlotIndexByName(FName WeaponName);
	UFUNCTION(BlueprintCallable)
	void AddAmmoSlot(EWeaponType WeaponAmmoType, FName WeaponName);
	void AddAmmoSlot(EWeaponType WeaponAmmoType, FName WeaponName, int32 BulletsCount, int32 MaxBulletsCount);
	//ToDo
	UFUNCTION(BlueprintCallable)
	bool AddBulletsToAmmoSlot(EWeaponType WeaponAmmoType, int32 BulletsCount, int32& BulletsOverFlowCount);
	UFUNCTION(BlueprintCallable)
	bool AddBulletsToCurrentAmmoSlot(int32 BulletsCount, int32& BulletsOverFlowCount);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetBulletsCountFromCurrentAmmoSlot();

	UFUNCTION(BlueprintCallable)
	void AddCurrentWeaponAmmo(int32 Bullets);
	UFUNCTION(BlueprintCallable)
	FHashCodeWeaponsInventory GetInventoryDataStruct();

	bool GetIsWeaponHasBeenInitialized(FName WeaponName);
	void SwitchFirstInitializationVarByName(FName WeaponsInventorySlotName, bool bIsFirstInit);
	void SwitchFirstInitializationVarByIndex(int32 WeaponsInventorySlotIndex, bool bIsFirstInit);

	bool SetWeaponAmmoInMagazineByName(FName WeaponName, int32 AmmoCount);
	int32 GetWeaponAmmoInMagazineByName(FName WeaponName);

	FName GetWeaponNameByType(EWeaponType WeaponType);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
