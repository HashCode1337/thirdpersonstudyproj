// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS/Character/TPSCharacter.h"
#include "TPS/Game/TPSGameInstance.h"
#include "TPS/Game/TPSPlayerController.h"
#include "TPS/Components/HealthComponent.h"
#include "TPS/GAS/TPSGameplayAbility.h"
#include "TPS/GAS/TPSAbSysComp.h"
#include "TPS/GAS/TPSAttributeSet.h"

#include "GameplayEffectTypes.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "UObject/ConstructorHelpers.h"

#include "Net/UnrealNetwork.h"

#include "Engine/World.h"

//#define DEB
#define CL(mtxt) UE_LOG(LogTemp, Warning, TEXT(mtxt))

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// AbilitySys
	AbilitySystemComp = CreateDefaultSubobject<UTPSAbSysComp>(TEXT("AbilitySystemComponent"));
	AbilitySystemComp->SetIsReplicated(true);
	AbilitySystemComp->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);

	Attributes = CreateDefaultSubobject<UTPSAttributeSet>(TEXT("Attributes"));

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}

	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> MaybeProneMontage(TEXT("AnimMontage'/Game/Blueprint/Character/Mannequin/Animations/NewAnim/NewFire_Rifle_Hip_Montage.NewFire_Rifle_Hip_Montage'"));
	if (MaybeProneMontage.Succeeded())
	{
		ProneAnimMontage = MaybeProneMontage.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> MaybeAimMontage(TEXT("AnimMontage'/Game/Blueprint/Character/Mannequin/Animations/NewAnim/NewFire_Rifle_Ironsights_Montage.NewFire_Rifle_Ironsights_Montage'"));
	if (MaybeProneMontage.Succeeded())
	{
		AimPressedAnimMontage = MaybeAimMontage.Object;
	}

	//AActor, OnTakeAnyDamage, AActor*, DamagedActor, float, Damage, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser
	this->OnTakeAnyDamage.AddDynamic(this, &ATPSCharacter::OnDamageReceived);

	SetReplicates(true);
	SetReplicateMovement(true);
}

void ATPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(ATPSCharacter, MovementState);
	//DOREPLIFETIME_CONDITION(ATPSCharacter, MovementState, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ATPSCharacter, RemoteActorYaw, COND_None);
}

UAbilitySystemComponent* ATPSCharacter::GetAbilitySystemComponent() const
{
	if (AbilitySystemComp)
	{
		return Cast<UAbilitySystemComponent>(AbilitySystemComp);
	}
	else
	{
		return nullptr;
	}
}

void ATPSCharacter::InitializeAttributes()
{
	if (AbilitySystemComp && DefaultAttributeEffect)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComp->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle SpecHandle = AbilitySystemComp->MakeOutgoingSpec(DefaultAttributeEffect, 1, EffectContext);
	
		if (SpecHandle.IsValid())
		{
			FActiveGameplayEffectHandle GEHandle = AbilitySystemComp->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		}
	}
}

void ATPSCharacter::GiveAbilities()
{
	if (HasAuthority() && AbilitySystemComp)
	{
		for (TSubclassOf<UTPSGameplayAbility>& StartupAbility : DefaultAbilities)
		{
			AbilitySystemComp->GiveAbility(
				FGameplayAbilitySpec(StartupAbility, 1, static_cast<int32>(StartupAbility.GetDefaultObject()->AbilityInputID), this)
			);
		}
	}
}

void ATPSCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AbilitySystemComp->InitAbilityActorInfo(this, this);

	InitializeAttributes();
	GiveAbilities();
}

void ATPSCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	AbilitySystemComp->InitAbilityActorInfo(this, this);

	InitializeAttributes();

	if (AbilitySystemComp && InputComponent)
	{
		const FGameplayAbilityInputBinds Binds("Confirm", "Cancel", "ETPSInputID", static_cast<int32>(ETPSInputID::Confirm), static_cast<int32>(ETPSInputID::Cancel));
		AbilitySystemComp->BindAbilityActivationToInputComponent(InputComponent,Binds);
	}
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr && IsLocallyControlled())
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	} 
	else if (CursorToWorld != nullptr && !IsLocallyControlled())
	{
		CursorToWorld->SetVisibility(false);
	}

	if (CameraBoom->TargetArmLength != SpringArmDesiredValue)
	{
		CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength, SpringArmDesiredValue, DeltaSeconds, SpringArmZoomSpeed);
	}


	SprintHandle();
	MovementTick(DeltaSeconds);
	HandleWeaponPistolOrNot();
}

void ATPSCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATPSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATPSCharacter::InputAxisY);
	PlayerInputComponent->BindAxis("MouseWheel", this, &ATPSCharacter::InputAxisMouseWheel);

	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ATPSCharacter::WalkKeyPressed);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &ATPSCharacter::WalkKeyReleased);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ATPSCharacter::AimKeyPressed);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ATPSCharacter::AimKeyReleased);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATPSCharacter::FireKeyPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ATPSCharacter::FireKeyReleased);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ATPSCharacter::SprintKeyPressed);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ATPSCharacter::SprintKeyReleased);
	
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ATPSCharacter::WeaponReloadTry);
	PlayerInputComponent->BindAction("DropWeapon", IE_Pressed, this, &ATPSCharacter::DropCurrentWeapon);
	PlayerInputComponent->BindAction("UseAction", IE_Pressed, this, &ATPSCharacter::UseAction);

	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ATPSCharacter::NextWeapon);
	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &ATPSCharacter::PrevWeapon);
}
void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	MyPlayerController = Cast<ATPSPlayerController>(GetController());

	UInventoryComponent* InventoryComp = FindComponentByClass<UInventoryComponent>();
	if (InventoryComp)
	{
		SetInventoryComponent(InventoryComp);
	}

	UHealthComponent* HealthComp = FindComponentByClass<UHealthComponent>();
	if (HealthComp)
	{
		SetHealthComponent(HealthComp);
	}
}

void ATPSCharacter::NextWeapon()
{
	if (GetIsActorDead()) return;

	if (bHasInventoryComponent && InventoryComponent)
	{
		InventoryComponent->SwitchNextWeapon();
		OnSwitchWeapon(GetCurrentWeaponName());
	}
}

void ATPSCharacter::PrevWeapon()
{
	if (GetIsActorDead()) return;

	if (bHasInventoryComponent && InventoryComponent)
	{
		InventoryComponent->SwitchPrevWeapon();
		OnSwitchWeapon(GetCurrentWeaponName());
	}
}

void ATPSCharacter::SetInventoryComponent(UInventoryComponent* NewInventoryComponent)
{
	if (NewInventoryComponent)
	{
		InventoryComponent = NewInventoryComponent;
		bHasInventoryComponent = true;
	}
}

void ATPSCharacter::SetHealthComponent(UHealthComponent* NewHealthComponent)
{
	if (NewHealthComponent)
	{
		bHasHealthComponent = true;
		HealthComponent = NewHealthComponent;
	}
}

void ATPSCharacter::UseAction()
{
	TArray<AActor*> PotentialInteracts = MyPlayerController->GetInteractableActorsList();
	TArray<AActor*> Interactable;
	IInterfaceHelper* InteractableObject = nullptr;
	
	bIsLastInteractionWasSuccess = false;

	for (AActor* el : PotentialInteracts)
	{
		InteractableObject = Cast<IInterfaceHelper>(el);

		if (InteractableObject)
		{
			Interactable.Add(el);
		}
	}

	if (Interactable.Num() > 0)
	{
		ATPSCharacter* MyChar = Cast<ATPSCharacter>(this);
		bIsLastInteractionWasSuccess = ApplyInteraction(Interactable, MyChar);
	}
}

bool ATPSCharacter::ApplyInteraction(TArray<AActor*> InteractableObjects, ATPSCharacter* ActionInstigator)
{
	bool result = false;

	//We take just first element from an array, because only this way is ok

	if (!InteractableObjects.IsValidIndex(0)) 
		return false;	

	IInterfaceHelper* InteractableElement = Cast<IInterfaceHelper>(InteractableObjects[0]);
	EInteractionType InteractType = InteractableElement->GetInteractionType();

	switch (InteractType)
	{
	case EInteractionType::None:
		UE_LOG(LogTemp, Warning, TEXT("NONE"));
		result = false;
		break;
	case EInteractionType::Pickup:
		UE_LOG(LogTemp, Warning, TEXT("PICKUP"));
		InteractableElement->ExecuteInteract(ActionInstigator);
		break;
	case EInteractionType::Use:
		UE_LOG(LogTemp, Warning, TEXT("USE"));
		//TODO
		break;
	default:
		break;
	}

	return result;
}

void ATPSCharacter::DropCurrentWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->DropWeaponFromHands();
		CurrentWeapon = nullptr;
	}

	if (InventoryComponent && bHasInventoryComponent)
	{
		InventoryComponent->DeleteWeaponFromInventory(InventoryComponent->GetCurrentSelectedSlot());
		InventoryComponent->SetCurrentSelectedSlot(-1);
	}
}

void ATPSCharacter::InputAxisX(float Value) { if (GetIsActorDead()) return; AxisX = Value; }
void ATPSCharacter::InputAxisY(float Value) { if (GetIsActorDead()) return; AxisY = Value; }

void ATPSCharacter::WalkKeyPressed()
{
	bWalkPressed = true;
	if (MovementState != EMovementState::Aim_State && MovementState != EMovementState::Walk_State)
	{
		ChangeMovementState(this, EMovementState::Walk_State);
	}
}
void ATPSCharacter::WalkKeyReleased()
{
	bWalkPressed = false;
	if (MovementState == EMovementState::Walk_State)
	{
		if (bAimPressed)
		{
			ChangeMovementState(this, EMovementState::Aim_State);
		}
		else
		{
			ChangeMovementState(this, EMovementState::Run_State);
		}
	}
}
void ATPSCharacter::AimKeyPressed()
{
	bAimPressed = true;
	if (MovementState != EMovementState::Aim_State)
	{
		ChangeMovementState(this, EMovementState::Aim_State);
	}
}
void ATPSCharacter::AimKeyReleased()
{
	bAimPressed = false;
	if (MovementState == EMovementState::Aim_State)
	{
		if (bWalkPressed)
		{
			ChangeMovementState(this, EMovementState::Walk_State);
		}
		else
		{
			ChangeMovementState(this, EMovementState::Run_State);
		}
	}
}
void ATPSCharacter::SprintKeyPressed() { bSprintPressed = true; }
void ATPSCharacter::SprintKeyReleased() { bSprintPressed = false; }
void ATPSCharacter::FireKeyPressed()
{
	if (GetIsActorDead()) return;

	if (CurrentWeapon)
	{
		if(CurrentWeapon->IsWeaponReadyToFire())
		{
			CurrentWeapon->SetWeaponFiring(true);
		}
	}
}
void ATPSCharacter::FireKeyReleased()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->SetWeaponFiring(false);
	}
}
void ATPSCharacter::InputAxisMouseWheel(float Value)
{
	if (GetIsActorDead()) return;

	Value *= SpringArmScrollMultiplier*-1;
	if (SpringArmDesiredValue > MinSpringArmDistance && SpringArmDesiredValue < MaxSpringArmDistance)
	{
		SpringArmDesiredValue += Value;
	}
	else if (SpringArmDesiredValue <= MinSpringArmDistance)
	{
		SpringArmDesiredValue = MinSpringArmDistance + 1;
	}
	else if (SpringArmDesiredValue >= MaxSpringArmDistance)
	{
		SpringArmDesiredValue = MaxSpringArmDistance - 1;
	}

	if (Value != 0)
	{
#ifdef DEB
		UE_LOG(LogTemp, Warning, TEXT("CurrentZoom - %f"), SpringArmDesiredValue);
#endif
	}
	
}
void ATPSCharacter::HandleWeaponPistolOrNot()
{
	if (CurrentWeapon)
	{
		UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
		FWeaponInfo WeaponInfoResult;

		if (GameInstance)
		{
			bool IsFind = GameInstance->GetWeaponInfoByName(GetCurrentWeaponName(), WeaponInfoResult);
			if (IsFind)
			{
				FName CurWepName = GetCurrentWeaponName();

				if (WeaponInfoResult.bIsWeaponLikeAPistolType)
				{
					SetCurrentWeaponTypeLikePistol(true);
				}
				else
				{
					SetCurrentWeaponTypeLikePistol(false);
				}
			}
		}
	}
}
void ATPSCharacter::InitActorWeapon(FName IdWeapon)
{
	UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo WeaponInfoResult;

	if (GameInstance)
	{
		bool IsFind = GameInstance->GetWeaponInfoByName(IdWeapon, WeaponInfoResult);
	}

	if (WeaponInfoResult.WeaponClass)
	{

		if (CurrentWeapon)
		{
			if (bHasInventoryComponent)
			{
				/*if (InventoryComponent)
				{
					InventoryComponent->AddWeaponToInventory(IdWeapon);
				}*/
			}
			else
			{
				return;
			}
			
			//CurrentWeapon->DropWeaponFromHands();
			CurrentWeapon->Destroy(true);
			CurrentWeapon = nullptr;
		}

		FVector SpawnLoc = FVector(0);
		FRotator SpawnRot = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponInfoResult.WeaponClass,&SpawnLoc,&SpawnRot,SpawnParams));
		
		CurrentWeapon = MyWeapon;

		if (CurrentWeapon)
		{
			const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			CurrentWeapon->AttachToComponent(GetMesh(), Rule, FName("hand_rSocket"));
			CurrentWeapon->SetWeaponOwner(this);

			bool bWillWeSetMaxAmmoOrJustFromInventory = GetInventoryComponent()->GetIsWeaponHasBeenInitialized(IdWeapon);
			if (bWillWeSetMaxAmmoOrJustFromInventory)
			{
				WeaponInfoResult.CurrentRoundCount = GetInventoryComponent()->GetWeaponAmmoInMagazineByName(IdWeapon);
				GetInventoryComponent()->SwitchFirstInitializationVarByName(IdWeapon, true);
			}
			else
			{
				WeaponInfoResult.CurrentRoundCount = WeaponInfoResult.MaxRound;
				GetInventoryComponent()->SwitchFirstInitializationVarByName(IdWeapon, true);
			}

			UE_LOG(LogTemp, Warning, TEXT("AmmoInMag - %i"), WeaponInfoResult.CurrentRoundCount);

			CurrentWeapon->InitWeapon(WeaponInfoResult);

			CurrentWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
			CurrentWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadFinish);

			CurrentWeaponName = IdWeapon;
		}
	}
}
bool ATPSCharacter::AddWeaponToActor(FName IdWeapon, bool PutInHands = false)
{
	bool Result = true;
	
	UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo WeaponInfoResult;

	if (GameInstance)
	{
		bool IsFind = GameInstance->GetWeaponInfoByName(IdWeapon, WeaponInfoResult);
	}

	if (InventoryComponent && bHasInventoryComponent)
	{
		if (InventoryComponent->HasFreeSpace())
		{
			InventoryComponent->AddWeaponToInventory(IdWeapon);
			UE_LOG(LogTemp, Warning, TEXT("AddWeaponToActor - weapon %s added to Actor"), *IdWeapon.ToString());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AddWeaponToActor failed, Actor doesn't have enough free space"));
			return false;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AddWeaponToActor failed, Actor doesn't have inventoryComponent"));
		return false;
	}

	if (PutInHands)
	{
		if (WeaponInfoResult.WeaponClass)
		{

			if (CurrentWeapon)
			{
				CurrentWeapon->Destroy(true);
				CurrentWeapon = nullptr;
			}

			FVector SpawnLoc = FVector(0);
			FRotator SpawnRot = FRotator(0);

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponInfoResult.WeaponClass, &SpawnLoc, &SpawnRot, SpawnParams));

			CurrentWeapon = MyWeapon;

			if (CurrentWeapon)
			{
				const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
				CurrentWeapon->AttachToComponent(GetMesh(), Rule, FName("hand_rSocket"));
				CurrentWeapon->SetWeaponOwner(this);

				const FName nme = CurrentWeapon->GetClass()->GetFName();

				CurrentWeapon->InitWeapon(WeaponInfoResult);

				CurrentWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
				CurrentWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadFinish);

				if (PutInHands)
				{
					InventoryComponent->SetCurrentSelectedSlot(InventoryComponent->Inventory.InventoryData.Num()); //Make public function for this
				}

				CurrentWeaponName = IdWeapon;
				OnSwitchWeapon(IdWeapon);
			}
		}
	}
	
	return Result;
}

bool ATPSCharacter::AddWeaponToActor(FName IdWeapon, bool PutInHands, int32 Rounds)
{
	bool result = AddWeaponToActor(IdWeapon, PutInHands);
	CurrentWeapon->SetCurrentRounds(Rounds);

	OnSwitchWeapon(IdWeapon);

	return result;
}

AWeaponBase* ATPSCharacter::GetCurrentWeapon()
{
	if (CurrentWeapon)
	{
		return CurrentWeapon;
	}
	else
	{
		return nullptr;
	}
}

FName ATPSCharacter::GetCurrentWeaponName() { return CurrentWeaponName; }

void ATPSCharacter::SetCurrentWeaponTypeLikePistol(bool CurWeapLikePistol) { bIsCurrentWeaponTypeLikePistol = CurWeapLikePistol; }

bool ATPSCharacter::GetCurrentWeaponTypeLikePistol() { return bIsCurrentWeaponTypeLikePistol; }

void ATPSCharacter::MovementTick(float DeltaTime)
{
	if (GetIsActorDead()) return;

	if (IsLocallyControlled())
	{
		AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
		AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (MyController)
		{
			FHitResult HitResult;
			MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);
			float LocYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.f, LocYaw, 0.f)));

			RemoteActorYaw = static_cast<uint8>(LocYaw + 180);

			FString mes = "CharRot - " + FString::FromInt(LocYaw + 180);
			GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Yellow, mes);
		}
	}
	else
	{
		FString mes = "RemoteCharRot - " + FString::FromInt(RemoteActorYaw + 180);
		GEngine->AddOnScreenDebugMessage(0, 0, FColor::Orange, mes);
	}
}

void ATPSCharacter::SprintHandle()
{
	if (!IsLocallyControlled()) return;
	if (IsCanSprint())
	{
		if (bSprintPressed)
		{
			ChangeMovementState(this, EMovementState::Sprint_State);
		}
	}
	else
	{
		if (bWalkPressed && MovementState != EMovementState::Walk_State)
		{
			ChangeMovementState(this, EMovementState::Walk_State);
		}
		else if (bAimPressed && MovementState != EMovementState::Aim_State)
		{
			ChangeMovementState(this, EMovementState::Aim_State);
		}
		else if (!bAimPressed && !bWalkPressed && MovementState != EMovementState::Run_State)
		{
			ChangeMovementState(this, EMovementState::Run_State);
		}
	}
}

bool ATPSCharacter::IsCanSprint()
{

	if (!GetController()) return false;
	if (!GetController()->IsLocalController()) return false;
	if (!GetController()->GetPawn()) return false;

	if (MovementState == EMovementState::Aim_State)
	{
		return false;
	}
	else if (MovementState == EMovementState::Walk_State)
	{
		return false;
	}
	else if (MovementState == EMovementState::Run_State || MovementState == EMovementState::Sprint_State)
	{
		FVector ActorVelocity = GetController()->GetPawn()->GetVelocity();
		FRotator ActorRotation = GetController()->GetPawn()->GetActorRotation();
		float ActorRelativeAngle = AnimInstance->CalculateDirection(ActorVelocity, ActorRotation);
		if (ActorRelativeAngle > -10 && ActorRelativeAngle < 10)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool ATPSCharacter::GetIsAimPressed()
{
	return bAimPressed;
}

void ATPSCharacter::ChangeMovementState_Implementation(ATPSCharacter* Char, EMovementState NewMovementState)
{

	if (!HasAuthority()) return;
	/*if (!Char->GetController()->IsLocalPlayerController())
	{
	}*/
	FString mes = "Char - " + Char->GetName() + " state - " + FString::FromInt(static_cast<int32>(NewMovementState));
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Cyan, mes);

	if (!Char) return;
	if (Char->MovementState == NewMovementState) return;
	Char->MovementState = NewMovementState;

	ApplyNet_ChangeMovementState(Char, NewMovementState);

	//if (HasAuthority()) ChangeMovementStateServer(MovementState);
}

void ATPSCharacter::ApplyNet_ChangeMovementState_Implementation(ATPSCharacter* Target, EMovementState NewMovementState)
{
	if (!Target) return;

	FString mes = "NetChar - " + Target->GetName() + " NewState - " + FString::FromInt(static_cast<int32>(NewMovementState));
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Red, mes);

	float ResSpeed = 600.f;
	if (Target)
	{
		switch (NewMovementState)
		{
		case EMovementState::Aim_State:
			ResSpeed = MovementInfo.AimSpeed;
			break;
		case EMovementState::Walk_State:
			ResSpeed = MovementInfo.WalkSpeed;
			break;
		case EMovementState::Run_State:
			ResSpeed = MovementInfo.RunSpeed;
			break;
		case EMovementState::Sprint_State:
			ResSpeed = MovementInfo.SprintSpeed;
			break;
		default:
			break;
		}

		Target->GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	}

}

void ATPSCharacter::ChangeMovementStateServer(EMovementState NewMovementState)
{

	UE_LOG(LogTemp, Warning, TEXT("ChangedOnServer"));

	int32 i = 0;
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		ATPSPlayerController* PC = Cast<ATPSPlayerController>(It->Get());
		ATPSCharacter* MC = Cast<ATPSCharacter>(PC->GetCharacter());
		i++;

		float ResSpeed = 600.f;
		if (MC)
		{
			switch (MC->MovementState)
			{
			case EMovementState::Aim_State:
				ResSpeed = MovementInfo.AimSpeed;
				break;
			case EMovementState::Walk_State:
				ResSpeed = MovementInfo.WalkSpeed;
				break;
			case EMovementState::Run_State:
				ResSpeed = MovementInfo.RunSpeed;
				break;
			case EMovementState::Sprint_State:
				ResSpeed = MovementInfo.SprintSpeed;
				break;
			default:
				break;
			}

			MC->GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
		}

		//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Cyan, FString::FromInt(static_cast<int32>(NewMovementState)) + " " + FString::FromInt(GetCharacterMovement()->MaxWalkSpeed), true);
		//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Cyan, MC->GetName(), true);
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1, FColor::Cyan, FString::FromInt(i), true);
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Animation)
{
	PlayAnimMontage(Animation);
	if (CurrentWeapon && CurrentWeapon->WeaponSettings.SoundReloadWeaponStart)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), CurrentWeapon->WeaponSettings.SoundReloadWeaponStart, CurrentWeapon->GetActorLocation());
	}
	if (CurrentWeapon)
	{
		CurrentWeapon->SkeletalMeshWeapon->HideBoneByName(FName("Clip_Bone"), EPhysBodyOp::PBO_MAX);
	}
}

void ATPSCharacter::WeaponReloadFinish()
{
	if (CurrentWeapon && CurrentWeapon->WeaponSettings.SoundReloadWeaponEnd)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), CurrentWeapon->WeaponSettings.SoundReloadWeaponEnd, CurrentWeapon->GetActorLocation());
	}
	if (CurrentWeapon)
	{
		CurrentWeapon->SkeletalMeshWeapon->UnHideBoneByName(FName("Clip_Bone"));
	}
}

void ATPSCharacter::WeaponReloadTry()
{
	if (GetIsActorDead()) return;

	if(CurrentWeapon && !CurrentWeapon->IsWeaponReloading())
	{
		CurrentWeapon->WeaponReloadingStart();
	}
}

UInventoryComponent* ATPSCharacter::GetInventoryComponent()
{
	if (InventoryComponent)
		return InventoryComponent;
	else
		return nullptr;
}

bool ATPSCharacter::GetIsActorDead()
{
	if (HealthComponent)
	{
		return !HealthComponent->GetIsAlive();
	}
	else
	{
		return false;
	}
}

void ATPSCharacter::SetIsActorDead(bool dead)
{
	if (HealthComponent)
	{
		HealthComponent->SetHealth(0,this);
	}
}

//ToDo
//void ATPSCharacter::OnDamageReceived(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
//{
//	
//}

void ATPSCharacter::OnDamageReceived_Implementation(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (HealthComponent)
	{
		FHashCodeDamageDetails DamageDetails;
		if (IsValid(InstigatedBy))
		{
			DamageDetails.Instigator = InstigatedBy->GetPawn();
		}
		else
		{
			return;
		}

		IInterfaceHelper* CastedCauser = Cast<IInterfaceHelper>(InstigatedBy->GetPawn());
		if (CastedCauser)
		{
			DamageDetails.WeaponName = CastedCauser->GetCurrentWeaponName();
		}

		DamageDetails.DamageType = EHashCodeDamageType::InstigatorShot;
		HealthComponent->AddDamage(Damage, DamageDetails);
	}
}

void ATPSCharacter::PlayFireMontage()
{
	if(CurrentWeapon)
	{	
		if (!CurrentWeapon->IsWeaponReloading())
		{
			UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetGameInstance());
			FWeaponInfo WeaponInfoResult;

			if (GameInstance)
			{
				bool IsFind = GameInstance->GetWeaponInfoByName(GetCurrentWeaponName(), WeaponInfoResult);
				if (IsFind)
				{
					FName CurWepName = GetCurrentWeaponName();
					if (bAimPressed)
					{
						if (WeaponInfoResult.AnimCharFireAim != nullptr)
							PlayAnimMontage(WeaponInfoResult.AnimCharFireAim);
					}
					else
					{
						if (WeaponInfoResult.AnimCharFire != nullptr)
							PlayAnimMontage(WeaponInfoResult.AnimCharFire);
					}
				}
			}
		}
	}
}

FVector ATPSCharacter::GetCursorLocation()
{
	return CursorToWorld->GetComponentLocation();
}

EMovementState ATPSCharacter::GetCurrentDispersion()
{
	return MovementState;
}

bool ATPSCharacter::CanWeaponOwnerDoFire()
{
	if (MovementState == EMovementState::Sprint_State)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void ATPSCharacter::OnSwitchWeapon_Implementation(FName WeaponName)
{
	
}

void ATPSCharacter::OnFireWeapon_Implementation(int32 BulletsCount, FVector BulletStartPos, FRotator BulletStartRot, AProjectileBase* BulletObject, AActor* ShellObject, FWeaponInfo WeaponInfo)
{
	//UE_LOG(LogTemp, Warning, TEXT("Fire weapon"));
}

