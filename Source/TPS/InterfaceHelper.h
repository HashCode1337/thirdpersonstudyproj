// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS/FuncLibrary/Types.h"
#include "InterfaceHelper.generated.h"

class UInventoryComponent;
class ATPSCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterfaceHelper : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API IInterfaceHelper
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void PlayFireMontage();
	virtual FVector GetCursorLocation();
	virtual EMovementState GetCurrentDispersion();
	virtual bool CanWeaponOwnerDoFire();
	virtual void OnFireWeapon(int32 BulletsCount, FVector BulletStartPos, FRotator BulletStartRot, AProjectileBase* BulletObject, AActor* ShellObject, FWeaponInfo WeaponInfo);
	virtual void OnSwitchWeapon(FName WeaponName);
	virtual FName GetCurrentWeaponName();
	virtual UInventoryComponent* GetInventoryComponent();
	virtual EInteractionType GetInteractionType();

	virtual bool ExecuteInteract(ATPSCharacter* ActionInstigator);
};
