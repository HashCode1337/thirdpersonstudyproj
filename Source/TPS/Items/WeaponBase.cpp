// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Items/WeaponBase.h"
#include "TPS/Game/TPSGameInstance.h"
#include "TPS/Components/InventoryComponent.h"
#include "Components/SkinnedMeshComponent.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootComp"));
	RootComponent = RootComp;

	//Init Comps
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalWeapon"));
	SkeletalMeshWeapon->SetupAttachment(RootComp);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticWeapon"));
	ProjectileSpawnLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileLocation"));

	//UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetGameInstance());
	//if (GameInstance)
	//{
		//FName ThisName = StaticClass()->GetFName();
		//UE_LOG(LogTemp, Warning, TEXT("This Name %s"),*ThisName.ToString());
	//}
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	if (!SkeletalMeshWeapon)
		SkeletalMeshWeapon->DestroyComponent(true);

	if (!StaticMeshWeapon)
		StaticMeshWeapon->DestroyComponent(true);

}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FString deltat = FString::SanitizeFloat(DeltaTime);
	GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Blue, "DeltaTime - " + deltat, false, FVector2D(1, 1));
	float framesT = UGameplayStatics::GetUnpausedTimeSeconds(GetWorld());
	FString framesStr = FString::SanitizeFloat(framesT);
	GEngine->AddOnScreenDebugMessage(2, 1.f, FColor::Red, "UpTime - " + framesStr, false, FVector2D(1, 1));
	FString LastShootTimeStr = FString::SanitizeFloat(LastShootTime);
	GEngine->AddOnScreenDebugMessage(3, 1.f, FColor::Red, "LastShoot - " + LastShootTimeStr, false, FVector2D(1, 1));
	/*WeaponHeightFromFloor = SkeletalMeshWeapon->GetSocketLocation("MuzzleFlash").Z - GetWeaponOwner()->GetActorLocation().Z;
	FString hghtstr = FString::SanitizeFloat(WeaponHeightFromFloor);
	GEngine->AddOnScreenDebugMessage(4, 1.f, FColor::Yellow, "WeaponHeightAboveFloor - " + hghtstr, false);*/

	if(bIsWeaponFiring)
	{
		IInterfaceHelper* WeapOwner = Cast<IInterfaceHelper>(GetWeaponOwner());
		if (WeapOwner && WeapOwner->CanWeaponOwnerDoFire())
		{
			FireWeapon();
		}
	}
}

void AWeaponBase::InitWeapon(FWeaponInfo NewWeaponInfo)
{
	WeaponSettings = NewWeaponInfo;
}

void AWeaponBase::FireWeapon()
{
	const bool FastConditionVar = WeaponFireCooldownFinished && WeaponSettings.CurrentRoundCount > 0 && !IsWeaponReloading() && !bBlockFire;
	for (uint8 BulletsInt = 0; BulletsInt < WeaponSettings.BulletsPerShot; BulletsInt++)
	{
		FVector Dir;
		FVector EndLocation = GetPlayerCursorLocationWithDispersion(Dir);

		if (FastConditionVar)
		{
			int32 BulPerShot = WeaponSettings.BulletsPerShot;

			FVector BulletStartPos = SkeletalMeshWeapon->GetSocketLocation("MuzzleFlash");
			FRotator BulletStartRot = SkeletalMeshWeapon->GetSocketRotation("MuzzleFlash");

			FMatrix BulletMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			BulletStartRot = BulletMatrix.Rotator();

			const FProjectileInfo ProjectileInfo = WeaponSettings.ProjectileSettings;

			FTransform BulletTransform = FTransform(BulletStartPos);
			FActorSpawnParameters SpawnParams;

			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			AProjectileBase* Bullet = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &BulletStartPos, &BulletStartRot, SpawnParams));
			Bullet->SetActorEnableCollision(true);
			Bullet->InitBullet(ProjectileInfo);

			IInterfaceHelper* OwnerCharacterCasted = Cast<IInterfaceHelper>(GetWeaponOwner());
			if (OwnerCharacterCasted)
			{
				OwnerCharacterCasted->PlayFireMontage();
				LastShootTime = UGameplayStatics::GetUnpausedTimeSeconds(GetWorld());
				
				// Hit one times, even it is iterated shotgun loop
				if (BulletsInt == BulPerShot - 1) 
				{
					WeaponSettings.CurrentRoundCount += -1;
					FiredBulletsFromCurrentMagazine += 1;
					FiredBulletsAtAll += 1;
					
					UInventoryComponent* Inventory = OwnerCharacterCasted->GetInventoryComponent();
					if (Inventory)
					{
						Inventory->SetWeaponAmmoInMagazineByName(OwnerCharacterCasted->GetCurrentWeaponName(), WeaponSettings.CurrentRoundCount);
					}

					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon, BulletStartPos, BulletStartRot);
					UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon, BulletStartPos, BulletStartRot);

					if (WeaponSettings.MeshShellOnShoot)
					{
						FTransform ShellTransform = SkeletalMeshWeapon->GetSocketTransform(FName("AmmoEject"));
						FActorSpawnParameters SpParams;
						SpParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
						AActor* BulletShell = GetWorld()->SpawnActor(WeaponSettings.MeshShellOnShoot, &ShellTransform, SpParams);
						BulletShell->SetLifeSpan(5.f);
						OwnerCharacterCasted->OnFireWeapon(WeaponSettings.CurrentRoundCount, BulletStartPos, BulletStartRot, Bullet, BulletShell, WeaponSettings);
					}
					else
					{
						OwnerCharacterCasted->OnFireWeapon(WeaponSettings.CurrentRoundCount, BulletStartPos, BulletStartRot, Bullet, nullptr, WeaponSettings);
					}
				}
			}

			SetWeaponFireCooldownFinished(false);
			GetWorld()->GetTimerManager().SetTimer(FireCooldownManager, this, &AWeaponBase::SetWeaponFireCooldownReady, WeaponSettings.RateOfFire, false);
		}
	}
}

void AWeaponBase::SetWeaponOwner(AActor* NewOwner)
{
	WeaponOwner = NewOwner;
}

AActor* AWeaponBase::GetWeaponOwner()
{
	if (WeaponOwner)
		return WeaponOwner;

	return nullptr;
}

void AWeaponBase::SetWeaponFireCooldownFinished(bool CooldownFinished)
{
	WeaponFireCooldownFinished = CooldownFinished;
}

void AWeaponBase::SetWeaponFireCooldownReady()
{
	WeaponFireCooldownFinished = true;
}

bool AWeaponBase::IsWeaponReadyToFire()
{
	return WeaponFireCooldownFinished;
}

bool AWeaponBase::IsWeaponReloading()
{
	return bIsWeaponReloading;
}

void AWeaponBase::SetWeaponFiring(bool Firing)
{
	bIsWeaponFiring = Firing;
}

int32 AWeaponBase::GetCurrentRounds()
{
	return WeaponSettings.CurrentRoundCount;
}

void AWeaponBase::SetCurrentRoundsMax()
{
	WeaponSettings.CurrentRoundCount = WeaponSettings.MaxRound;
}
void AWeaponBase::SetCurrentRounds(int32 RoundsCount)
{
	WeaponSettings.CurrentRoundCount = RoundsCount;
}

void AWeaponBase::WeaponReloadingStart()
{
	if (WeaponSettings.ReloadTime)
	{
		bIsWeaponReloading = true;
		float ReloadTime = WeaponSettings.ReloadTime;
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharReload);
		FTimerHandle ReloadHandler;
		GetWorld()->GetTimerManager().SetTimer(ReloadHandler, this, &AWeaponBase::WeaponReloadingFinish, WeaponSettings.ReloadTime, false);
		DropMagazineFromReloadingSlot();
	}
}

void AWeaponBase::WeaponReloadingFinish()
{
	bIsWeaponReloading = false;

	CalculateFiredBulletsAndReset();

	OnWeaponReloadEnd.Broadcast();

	IInterfaceHelper* CastedWeaponOwner = Cast<IInterfaceHelper>(WeaponOwner);

	if (CastedWeaponOwner)
		CastedWeaponOwner->OnSwitchWeapon(CastedWeaponOwner->GetCurrentWeaponName());
}

FVector AWeaponBase::GetPlayerCursorLocation()
{
	FVector EndLoc;

	IInterfaceHelper* IIH = Cast<IInterfaceHelper>(GetWeaponOwner());

	if (IIH)
		return IIH->GetCursorLocation() + FVector(0.f,0.f, WeaponHeightFromFloor);
	else
		return FVector::ZeroVector + FVector(0.f, 0.f, WeaponHeightFromFloor);
}

FVector AWeaponBase::GetPlayerCursorLocationWithDispersion(FVector& OutDirection)
{
	FVector BulletDestinationWithNoRecoil = GetPlayerCursorLocation();
	float CurShotRecoil = 0.f;

	IInterfaceHelper* IIH = Cast<IInterfaceHelper>(GetWeaponOwner());
	if (IIH)
	{
		EMovementState CurPlayerMovementState = IIH->GetCurrentDispersion();

		switch (CurPlayerMovementState)
		{
		case EMovementState::Aim_State:

			CurrentDispMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMax;
			CurrentDispMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMin;
			CurrentDispRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimRecoil;
			CurrentDispReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;

			break;
		case EMovementState::Walk_State:

			CurrentDispMax = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMax;
			CurrentDispMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMin;
			CurrentDispRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimRecoil;
			CurrentDispReduction = WeaponSettings.DispersionWeapon.Walk_StateDispersionReduction;

			break;
		case EMovementState::Run_State:

			CurrentDispMax = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMax;
			CurrentDispMin = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMin;
			CurrentDispRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionAimRecoil;
			CurrentDispReduction = WeaponSettings.DispersionWeapon.Run_StateDispersionReduction;

			break;
		case EMovementState::Sprint_State:

			break;
		default:
			break;
		}

		if (LastShootTime + WeaponSettings.DispersionWeapon.SingleShootCooldownWithNoRecoil > UGameplayStatics::GetUnpausedTimeSeconds(GetWorld()))
			CurShotRecoil = FMath::RandRange(CurrentDispMin, CurrentDispMax + CurrentDispRecoil);
		else
			CurShotRecoil = FMath::RandRange(CurrentDispMin, CurrentDispMax - CurrentDispReduction);
	}

	FVector Dir = (BulletDestinationWithNoRecoil - SkeletalMeshWeapon->GetSocketLocation("MuzzleFlash"));
	Dir.Normalize();

	if (WeaponSettings.bIsDebugMode)
		DrawDebugCone(GetWorld(), SkeletalMeshWeapon->GetSocketLocation("MuzzleFlash"), Dir, 1000.f , CurShotRecoil * PI / 180.f, CurShotRecoil * PI / 180.f, 32, FColor::Cyan, false, 3.f);

	OutDirection = FMath::VRandCone(Dir, CurShotRecoil * PI / 180.f);
	BulletDestinationWithNoRecoil = BulletDestinationWithNoRecoil + FMath::VRandCone(Dir, CurShotRecoil * PI / 180.f);


	return BulletDestinationWithNoRecoil;
}

void AWeaponBase::DropMagazineFromReloadingSlot()
{
	if (WeaponSettings.MeshMagazineOnDrop)
	{		
		FTransform MagTransform = SkeletalMeshWeapon->GetBoneTransform(SkeletalMeshWeapon->GetBoneIndex(FName("Clip_Bone")));

		FActorSpawnParameters SpParams;
		SpParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		AActor* Mag = GetWorld()->SpawnActor(WeaponSettings.MeshMagazineOnDrop, &MagTransform, SpParams);
		Mag->SetActorEnableCollision(true);
		Mag->SetLifeSpan(5.f);

	}
}

void AWeaponBase::DropWeaponFromHands()
{
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("Items"));
	SkeletalMeshWeapon->SetSimulatePhysics(true);
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	
	WeaponOwner = nullptr;
}

void AWeaponBase::CalculateFiredBulletsAndReset()
{
	IInterfaceHelper* CastedWeaponOwner = Cast<IInterfaceHelper>(WeaponOwner);

	if (CastedWeaponOwner)
	{
		UInventoryComponent* PlayerInventory = CastedWeaponOwner->GetInventoryComponent();
		if (PlayerInventory)
		{
			int32 BulletsOverflowed = 0;

			int32 BulletsExistInWeapon = WeaponSettings.CurrentRoundCount;
			int32 BulletsExistInInventory = PlayerInventory->GetBulletsCountFromCurrentAmmoSlot();
			int32 MaxBulletsInWeapon = WeaponSettings.MaxRound;
			int32 PotentialBulletsCountInWeapon = 0;

			int32 ShootedBullets = WeaponSettings.MaxRound - WeaponSettings.CurrentRoundCount;
			int32 BulletsNeedsToBeTaken = WeaponSettings.MaxRound - ShootedBullets;

			if (BulletsExistInWeapon > 0)
			{
				BulletsExistInInventory += BulletsExistInWeapon;
				PotentialBulletsCountInWeapon = 0;
			}

			if (BulletsExistInInventory <= MaxBulletsInWeapon)
			{
				PotentialBulletsCountInWeapon = BulletsExistInInventory;
			}
			else
			{
				PotentialBulletsCountInWeapon = MaxBulletsInWeapon;
			}

			PlayerInventory->AddBulletsToCurrentAmmoSlot(WeaponSettings.CurrentRoundCount - WeaponSettings.MaxRound, BulletsOverflowed);
			WeaponSettings.CurrentRoundCount = PotentialBulletsCountInWeapon;
			PlayerInventory->SetWeaponAmmoInMagazineByName(CastedWeaponOwner->GetCurrentWeaponName(), PotentialBulletsCountInWeapon);

			UE_LOG(LogTemp, Warning, TEXT("overflowed %i"), BulletsOverflowed);

			FiredBulletsFromCurrentMagazine = 0;
		}
	}
}

void AWeaponBase::PlayFireMontage()
{
	//Nothing to do here because made it for class interface
}


