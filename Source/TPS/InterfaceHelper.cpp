// Fill out your copyright notice in the Description page of Project Settings.


#include "InterfaceHelper.h"
#include "TPS/Components/InventoryComponent.h"

// Add default functionality here for any IInterfaceHelper functions that are not pure virtual.
void IInterfaceHelper::PlayFireMontage()
{
	
}

FVector IInterfaceHelper::GetCursorLocation()
{
	return FVector::ZeroVector;
}

EMovementState IInterfaceHelper::GetCurrentDispersion()
{
	return EMovementState::Walk_State;
}

bool IInterfaceHelper::CanWeaponOwnerDoFire()
{
	return false;
}

void IInterfaceHelper::OnFireWeapon(int32 BulletsCount, FVector BulletStartPos, FRotator BulletStartRot, AProjectileBase* BulletObject, AActor* ShellObject, FWeaponInfo WeaponInfo)
{

}

void IInterfaceHelper::OnSwitchWeapon(FName WeaponName)
{

}

FName IInterfaceHelper::GetCurrentWeaponName()
{
	return "None";
}

UInventoryComponent* IInterfaceHelper::GetInventoryComponent()
{
	return nullptr;
}

EInteractionType IInterfaceHelper::GetInteractionType()
{
	return EInteractionType::None;
}

bool IInterfaceHelper::ExecuteInteract(ATPSCharacter* ActionInstigator)
{
	return false;
}
