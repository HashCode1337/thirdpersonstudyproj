// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "TPS/Character/TPSCharacter.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UHealthComponent::SetHealth(float HealthVal, AActor* Instigator)
{
	if (!ComponentOwner) return;
	if (!Instigator)
	{
		UE_LOG(LogTemp, Warning, TEXT("ERROR - UHealthComponent::SetHealth aborted, Instigator is nullptr"));
	}

	FHashCodeDamageDetails DamageDetails;
	DamageDetails.DamageType = EHashCodeDamageType::ManualSet;
	DamageDetails.bIsDetailsHasInfo = true;
	DamageDetails.Instigator = Instigator;

	if (Health > 0 && Health <= 100 ) // Still alive
	{
		Health = HealthVal;
		OnDamageChanged.Broadcast(Instigator, Health - HealthVal, ComponentOwner, DamageDetails);
	}
	else if (HealthVal <= 0) // Death
	{
		Health = 0;
		bIsAlive = false;
		OnDamageChanged.Broadcast(Instigator, Health - HealthVal, ComponentOwner, DamageDetails);
		OnKilled.Broadcast(Instigator, ComponentOwner, DamageDetails);
		ATPSCharacter* CastedChar = Cast<ATPSCharacter>(ComponentOwner);
		if (CastedChar) CastedChar->UnPossessed();
	}
	else if (HealthVal > 100) // Max health
	{
		Health = 100;
		OnDamageChanged.Broadcast(Instigator, Health - HealthVal, ComponentOwner, DamageDetails);
	}
}

void UHealthComponent::AddDamage(float Damage, FHashCodeDamageDetails& DamageDetails)
{
	if (DamageDetails.Instigator == nullptr) { UE_LOG(LogTemp, Warning, TEXT("ERROR - UHealthComponent::AddDamage - You MUST specify instigator in DamageDetails Struct!!! Execution aborted")); return; }
	if (DamageDetails.WeaponName == "None") { UE_LOG(LogTemp, Warning, TEXT("ERROR - UHealthComponent::AddDamage - You MUST specify weaponName in DamageDetails Struct!!! Execution aborted")); return; }
	if (DamageDetails.DamageType == EHashCodeDamageType::Unknown) { UE_LOG(LogTemp, Warning, TEXT("ERROR - UHealthComponent::AddDamage - You MUST specify DamageType in DamageDetails Struct!!! Execution aborted")); return; }
	if (Damage == 0) return;

	float CalcDamageAfterArmor = Damage / Armor;

	DamageDetails.bIsDetailsHasInfo = true;
	DamageDetails.DamageCaused = CalcDamageAfterArmor;
	DamageDetails.DamageDealerPosition = DamageDetails.Instigator->GetActorLocation();
	DamageDetails.DamageDealerRotation = DamageDetails.Instigator->GetActorRotation();

	LashHitInstigator = DamageDetails.Instigator;
	LastHitTime = GetWorld()->GetUnpausedTimeSeconds();

	if (bIgnoreDamage)
	{
		DamageDetails.bIsDamageIgnored = true;
		return;
	}

	if (CalcDamageAfterArmor >= Health)
	{
		Health = 0;
		bIsAlive = false;

		OnDamageChanged.Broadcast(DamageDetails.Instigator, Damage, ComponentOwner, DamageDetails);
		OnKilled.Broadcast(DamageDetails.Instigator, ComponentOwner, DamageDetails);

		OnGetDamage(DamageDetails.Instigator, ComponentOwner, DamageDetails);
	}
	else if (CalcDamageAfterArmor < Health)
	{
		Health -= CalcDamageAfterArmor;
		OnDamageChanged.Broadcast(DamageDetails.Instigator, Damage, ComponentOwner, DamageDetails);
	}
}

void UHealthComponent::AllowDamage(bool bIgnoreDamageDealing)
{
	bIgnoreDamage = bIgnoreDamageDealing;
}

void UHealthComponent::SetArmor(float NewArmor)
{
	Armor = NewArmor;
}

void UHealthComponent::AddArmor(float ArmorToAdd)
{
	Armor += ArmorToAdd;
}

float UHealthComponent::GetDamage()
{
	return 100 - Health;
}

float UHealthComponent::GetHealth()
{
	return Health;
}

float UHealthComponent::GetHealthRanged()
{
	return UKismetMathLibrary::MapRangeClamped(Health, 0, 100, 0, 1);
}

float UHealthComponent::GetArmor()
{
	return Armor;
}

bool UHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

bool UHealthComponent::GetIsInvulnerable()
{
	return bIgnoreDamage;
}

void UHealthComponent::OnDeath_Implementation(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails)
{

}

void UHealthComponent::OnGetDamage_Implementation(AActor* Instigator, AActor* Victim, FHashCodeDamageDetails DamageDetails)
{

}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

