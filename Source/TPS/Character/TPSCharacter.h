// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Animation/AnimInstance.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"

//#include "TPS/GAS/TPSAbSysComp.h"
#include "TPS/Components/InventoryComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/InterfaceHelper.h"
#include "TPSCharacter.generated.h"

class UHealthComponent;
class ATPSPlayerController;

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public IInterfaceHelper, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	//GameplayAbilitySystem

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
	class UTPSAbSysComp* AbilitySystemComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
	class UTPSAttributeSet* Attributes;

	// Network
	//void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	
	UPROPERTY(ReplicatedUsing = OnRep_RemoteActorYaw)
	uint8 RemoteActorYaw = 0;
	uint8 RemoteActorYawOld = 0;

	UFUNCTION()
	void OnRep_RemoteActorYaw(uint8 Yaw);

public:
	ATPSCharacter();

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	virtual void InitializeAttributes();
	virtual void GiveAbilities();

	virtual void PossessedBy(AController* NewController) override;
	virtual void OnRep_PlayerState() override;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TSubclassOf<class UGameplayEffect> DefaultAttributeEffect;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TArray <TSubclassOf<class UTPSGameplayAbility>> DefaultAbilities;


	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void BeginPlay() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
	/*Anim montage on aim pressed*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* AimPressedAnimMontage;
	/*Anim montage on just standing*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* ProneAnimMontage;
	/*Anim montage on just standing*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* ReloadAnimMontage;

	ATPSPlayerController* MyPlayerController;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float MinSpringArmDistance = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float MaxSpringArmDistance = 1200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float SpringArmScrollMultiplier = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float SpringArmDesiredValue = 800.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float SpringArmZoomSpeed = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName StartWeaponClass = TEXT("Rifle");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	bool bHasInventoryComponent = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UInventoryComponent* InventoryComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	bool bHasHealthComponent = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	UHealthComponent* HealthComponent = nullptr;

	void NextWeapon();
	void PrevWeapon();
	UFUNCTION()
	void SetInventoryComponent(UInventoryComponent* NewInventoryComponent);
	UFUNCTION()
	void SetHealthComponent(UHealthComponent* NewHealthComponent);

	UAnimInstance* AnimInstance;

	UFUNCTION(BlueprintCallable)
	void UseAction();

	UFUNCTION()
	bool ApplyInteraction(const TArray<AActor*> InteractableObjects, ATPSCharacter* ActionInstigator);

	UPROPERTY()
	bool bIsLastInteractionWasSuccess = false;

	UFUNCTION()
	void DropCurrentWeapon();

	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void WalkKeyPressed();
	UFUNCTION()
	void WalkKeyReleased();
	UFUNCTION()
	void AimKeyPressed();    
	UFUNCTION()
	void AimKeyReleased();
	UFUNCTION()
	void SprintKeyPressed();
	UFUNCTION()
	void SprintKeyReleased();
	UFUNCTION()
	void FireKeyPressed();
	UFUNCTION()
	void FireKeyReleased();

	// Camera zoom
	void InputAxisMouseWheel(float Value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(BlueprintReadOnly)
	bool bWalkPressed = false;
	UPROPERTY(BlueprintReadOnly)
	bool bAimPressed = false;
	UPROPERTY(BlueprintReadOnly)
	bool bSprintPressed = false;
	bool bCanSprint = false;

	UFUNCTION()
	void HandleWeaponPistolOrNot();
	UFUNCTION(BlueprintCallable)
	void InitActorWeapon(FName IdWeapon);

	bool AddWeaponToActor(FName IdWeapon, bool PutInHands);
	UFUNCTION(BlueprintCallable)
	bool AddWeaponToActor(FName IdWeapon, bool PutInHands, int32 Rounds);

	UFUNCTION(BlueprintCallable)
	AWeaponBase* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	FName GetCurrentWeaponName();
	UPROPERTY()
	FName CurrentWeaponName;
	UPROPERTY()
	AWeaponBase* CurrentWeapon;


	UFUNCTION(BlueprintCallable)
	void SetCurrentWeaponTypeLikePistol(bool CurWeapLikePistol);
	UFUNCTION(BlueprintCallable)
	bool GetCurrentWeaponTypeLikePistol();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsCurrentWeaponTypeLikePistol = false;

	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION()
	void SprintHandle();
	UFUNCTION()
	bool IsCanSprint();

	UFUNCTION(BlueprintCallable)
	bool GetIsAimPressed();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ChangeMovementState(ATPSCharacter* Char, EMovementState NewMovementState);
	UFUNCTION (NetMulticast, Reliable)
	void ApplyNet_ChangeMovementState(ATPSCharacter* Target, EMovementState NewMovementState);
	
	UFUNCTION(BlueprintCallable)
	void ChangeMovementStateServer(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
	void WeaponReloadStart(UAnimMontage* Animation);
	UFUNCTION(BlueprintCallable)
	void WeaponReloadFinish();
	UFUNCTION(BlueprintCallable)
	void WeaponReloadTry();

public:
	UFUNCTION(BlueprintCallable)
	virtual	UInventoryComponent* GetInventoryComponent();
	UFUNCTION(BlueprintCallable)
	bool GetIsActorDead();
	UFUNCTION(BlueprintCallable)
	void SetIsActorDead(bool dead);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnDamageReceived(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	virtual void OnDamageReceived_Implementation(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	
	/*Extra interface funcs*/
	UFUNCTION()
	virtual void PlayFireMontage() override;
	FVector GetCursorLocation() override;
	EMovementState GetCurrentDispersion() override;
	bool CanWeaponOwnerDoFire() override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Character")
	void OnSwitchWeapon(FName WeaponName);
	void OnSwitchWeapon_Implementation(FName WeaponName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void OnFireWeapon(int32 BulletsCount, FVector BulletStartPos, FRotator BulletStartRot, AProjectileBase* BulletObject, AActor* ShellObject, FWeaponInfo WeaponInfo);
	void OnFireWeapon_Implementation(int32 BulletsCount, FVector BulletStartPos, FRotator BulletStartRot, AProjectileBase* BulletObject, AActor* ShellObject, FWeaponInfo WeaponInfo);



};

