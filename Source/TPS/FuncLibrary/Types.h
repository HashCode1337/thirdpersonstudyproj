// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TPS/Items/EjectThingsBase.h"
#include "Engine/Texture2D.h"
#include "Types.generated.h"

class AProjectileDefault;
class AWeaponBase;

UENUM(BlueprintType)
enum class ETPSInputID : uint8
{
	None UMETA(DisplayName = "None"),
	Confirm UMETA(DisplayName = "Confirm"),
	Cancel UMETA(DisplayName = "Cancel"),
	Key UMETA(DisplayName = "Key")
};

UENUM(BlueprintType)
enum class EHashCodeDamageType : uint8
{
	Unknown UMETA(DisplayName = "Unknown"),
	ManualSet UMETA(DisplayName = "ManualSet"),
	InstigatorShot UMETA(DisplayName = "InstigatorShot"),
	InstigatorExpl UMETA(DisplayName = "InstigatorExpl"),
	FallDamage UMETA(DisplayName = "FallDamage")
};

USTRUCT(BlueprintType)
struct FHashCodeDamageDetails
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsDetailsHasInfo = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EHashCodeDamageType DamageType = EHashCodeDamageType::Unknown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponName = "None";

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* Instigator = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector DamageDealerPosition = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator DamageDealerRotation = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DamageCaused = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsDamageIgnored = false;


};

UENUM(BlueprintType)
enum class EInteractionType : uint8
{
	None UMETA(DisplayName = "None"),
	Pickup UMETA(DisplayName = "Pickup"),
	Use UMETA(DisplayName = "Use")
};

UENUM(BlueprintType)
enum class EBoxColor : uint8
{
	Default UMETA(DisplayName = "Default"),
	Active UMETA(DisplayName = "Active")
};

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	TypeFree UMETA(DisplayName = "Free"),
	TypeRifle UMETA(DisplayName = "Rifle"),
	TypePistol UMETA(DisplayName = "Pistol"),
	TypeShotgun UMETA(DisplayName = "Shotgun"),
	TypeSniperRifle UMETA(DisplayName = "SniperRifle"),
	TypeGrenadeLauncher UMETA(DisplayName = "GrenadeLauncher")
};

USTRUCT(BlueprintType)
struct FHashCodeInventoryItemData
{
	GENERATED_BODY()

	//Maybe we don't need it? because of WeaponNameDataTable???
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Item")
	//TSubclassOf<AWeaponBase> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Item")
	FName WeaponNameInDataTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Item")
	int32 WeaponLoadedAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Item")
	bool IsFirstInitialization = false;
	
	//Why I did it?
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Item")
	//AWeaponBase* WeaponPtr = nullptr;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Ammo Stats")
	FName WeaponTypeFName = "Name";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Ammo Stats")
	EWeaponType WeaponType = EWeaponType::TypeRifle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Ammo Stats")
	int32 AmmoCount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Ammo Stats")
	int32 MaxAmmoCount = 0;
};


USTRUCT(BlueprintType)
struct FHashCodeWeaponsInventory
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	int32 InventoryCapacity = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	int32 CurrentSelectedSlot = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<FHashCodeInventoryItemData> InventoryData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<FAmmoSlot> InventoryAmmoData;


};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 800.f;

};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	TSubclassOf<class AProjectileBase> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	float ProjectileDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	float ProjectileLifeTime = 5.f;

	// decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;

	// particles on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitParticles;

	// sounds on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;

};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float SingleShootCooldownWithNoRecoil = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	bool bIsDebugMode = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	bool bIsWeaponLikeAPistolType = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	TSubclassOf<class AWeaponBase>	WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	float ReloadTime = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 MaxRound = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 MaxInventoryRound = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 CurrentRoundCount = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	int32 BulletsPerShot = 0.5;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings ")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	USoundBase* SoundFireWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	USoundBase* SoundReloadWeaponStart;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	USoundBase* SoundReloadWeaponEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	UParticleSystem* EffectFireWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Projectile Settings")
	FProjectileInfo ProjectileSettings;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	float WeaponTraceDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	float WeaponTraceDistance = 4000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Settings")
	UDecalComponent* DecalOnHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Char Anim Settings")
	UAnimMontage* AnimCharFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Char Anim Settings")
	UAnimMontage* AnimCharFireAim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Char Anim Settings")
	UAnimMontage* AnimCharReload;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Extras")
	TSubclassOf<AEjectThingsBase> MeshMagazineOnDrop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Extras")
	TSubclassOf<AEjectThingsBase> MeshShellOnShoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float SwitchWeaponTime = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	EWeaponType WeaponType = EWeaponType::TypeRifle;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 10;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Weapon Stats")
	FName WeaponName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Weapon Stats")
	FAdditionalWeaponInfo WeaponData;
};

UCLASS(BlueprintType)
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};