// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "DrawDebugHelpers.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

#include "TPS/FuncLibrary/Types.h"
#include "TPS/Items/ProjectileBase.h"
#include "TPS/InterfaceHelper.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);

UCLASS()
class TPS_API AWeaponBase : public AActor, public IInterfaceHelper
{
	GENERATED_BODY()
	
private:

	//Variables
	int32 FiredBulletsFromCurrentMagazine = 0;
	int32 FiredBulletsAtAll = 0;
	bool bIsWeaponReloading = false;
	bool bIsWeaponFiring = false;
	bool WeaponFireCooldownFinished = true;
	float WeaponHeightFromFloor = 0.f;
	FTimerHandle FireCooldownManager;
	
	//Variables
public:

	//Delegates
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Weapon Settings")
	class USceneComponent* RootComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Weapon Settings")
	class USkeletalMeshComponent* SkeletalMeshWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Weapon Settings")
	class UStaticMeshComponent* StaticMeshWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Weapon Settings")
	class UArrowComponent* ProjectileSpawnLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	FWeaponInfo WeaponSettings;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	FAdditionalWeaponInfo WeaponInfo;

	UPROPERTY()
	AActor* WeaponOwner;

	/*Dispersion settings*/
	bool bBlockFire = false;
	bool bReduceDisp = false;
	float CurrentDisp = 0.f;
	float CurrentDispMax = 1.f;
	float CurrentDispMin = 0.1f;
	float CurrentDispRecoil = 0.1f;
	float CurrentDispReduction = 0.1f;

	float LastShootTime = 0.f;

public:	
	// Sets default values for this actor's properties
	AWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*Init weapon settings with data table NewWeaponInfo*/
	virtual void InitWeapon(FWeaponInfo NewWeaponInfo);

	/*Do fire*/
	virtual void FireWeapon();

	/*Set weapon owner, just flag*/
	UFUNCTION(BlueprintCallable)
	virtual void SetWeaponOwner(AActor* NewOwner);
	
	/*Get weapon owner, just flag*/
	UFUNCTION(BlueprintCallable)
	virtual AActor* GetWeaponOwner();

	/*Switch weapon cooldown, ready or not*/
	UFUNCTION(BlueprintCallable)
	void SetWeaponFireCooldownFinished(bool CooldownFinished);

	/*Make cooldown finished, this function uses only with time manager*/
	UFUNCTION(BlueprintCallable)
	void SetWeaponFireCooldownReady();

	/*Return is weapon ready to fire or not*/
	UFUNCTION(BlueprintCallable)
	bool IsWeaponReadyToFire();

	/*Return is weapon ready to fire or not*/
	UFUNCTION(BlueprintCallable)
	bool IsWeaponReloading();

	/*Set weapon fire state, firing or not*/
	UFUNCTION(BlueprintCallable)
	void SetWeaponFiring(bool Firing);

	/*Get current loaded ammo*/
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentRounds();

	/*Set current max ammo*/
	UFUNCTION(BlueprintCallable)
	void SetCurrentRoundsMax();

	/*Set current ammo*/
	UFUNCTION(BlueprintCallable)
	void SetCurrentRounds(int32 RoundsCount);

	/*Start weapon reloading*/
	UFUNCTION()
	void WeaponReloadingStart();

	/*Start weapon reloading*/
	UFUNCTION()
	void WeaponReloadingFinish();

	/*Get player cursor location*/
	UFUNCTION()
	FVector GetPlayerCursorLocation();

	/*Get player cursor location*/
	UFUNCTION()
	FVector GetPlayerCursorLocationWithDispersion(FVector& OutDirection);

	/*Drop empty magazine after starting reload*/
	UFUNCTION()
	void DropMagazineFromReloadingSlot();

	/*Drop weapon which actor carries*/
	UFUNCTION()
	void DropWeaponFromHands();

	/*Calc bullets and subtract it from inventory*/
	UFUNCTION()
	void CalculateFiredBulletsAndReset();


public:
	
	/*Extra interface funcs */
	UFUNCTION()
	virtual void PlayFireMontage() override;
	
};
