// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Game/TPSGameInstance.h"
#include "TPS/Character/TPSCharacter.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

int32 UInventoryComponent::SwitchNextWeapon()
{
	ClearUnregistredItems();

	int32 ResultSlot = Inventory.CurrentSelectedSlot;

	if (ResultSlot + 1 < Inventory.InventoryData.Num())
		ResultSlot++;
	else
		ResultSlot = 0;

	if (!Inventory.InventoryData.IsValidIndex(ResultSlot))
		return -1;

	//UE_LOG(LogTemp, Warning, TEXT("Index Next - %i"), ResultSlot);
	Inventory.CurrentSelectedSlot = ResultSlot;

	SelectSlotAndApply(ResultSlot);

	//UE_LOG(LogTemp, Warning, TEXT("--- %i"), ResultSlot);
	/*for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		FString Wname = el.WeaponNameInDataTable.ToString();
		UE_LOG(LogTemp, Warning, TEXT("-- InvArray %s"), *Wname);
	}*/

	return ResultSlot;
}

int32 UInventoryComponent::SwitchPrevWeapon()
{
	ClearUnregistredItems();

	int32 ResultSlot = Inventory.CurrentSelectedSlot;

	if (ResultSlot - 1 < 0)
		ResultSlot = Inventory.InventoryData.Num() - 1;
	else
		ResultSlot--;

	if (!Inventory.InventoryData.IsValidIndex(ResultSlot))
		return -1;

	UE_LOG(LogTemp, Warning, TEXT("Index Prev - %i"), ResultSlot);

	Inventory.CurrentSelectedSlot = ResultSlot;

	SelectSlotAndApply(ResultSlot);

	UE_LOG(LogTemp, Warning, TEXT("--- %i"), ResultSlot);
	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		FString Wname = el.WeaponNameInDataTable.ToString();
		UE_LOG(LogTemp, Warning, TEXT("-- InvArray %s"), *Wname);
	}

	return ResultSlot;
}

bool UInventoryComponent::SelectSlotAndApply(int32 SlotNumber)
{
	bool Result = true;

	SetCurrentSelectedSlot(SlotNumber);

	if (SlotNumber == -1)
		return false;

	FName SelectedWeaponName = Inventory.InventoryData[SlotNumber].WeaponNameInDataTable;
	ATPSCharacter* ParentActor = Cast<ATPSCharacter>(InventoryOwner);

	if (ParentActor)
		ParentActor->InitActorWeapon(SelectedWeaponName);

	AWeaponBase* MyWeapon = ParentActor->GetCurrentWeapon();
	if (MyWeapon)
	{
		int32 CurrentAmmoIndex = FindWeaponSlotIndexByName(SelectedWeaponName);
		if (CurrentAmmoIndex != -1 && Inventory.InventoryData.IsValidIndex(CurrentAmmoIndex))
		{
			int32 SupposedAmmoCountInMagazine = Inventory.InventoryData[CurrentAmmoIndex].WeaponLoadedAmmo;
			MyWeapon->SetCurrentRounds(SupposedAmmoCountInMagazine);
		}
	}

	return Result;
}

bool UInventoryComponent::AddWeaponToInventory(FName WeaponName, bool TakeInHands)
{
	if (!HasFreeSpace())
		return false;

	if (IsWeaponAlreadyExist(WeaponName))
		return false;
	

	ATPSCharacter* ParentActor = Cast<ATPSCharacter>(InventoryOwner);

	FHashCodeInventoryItemData NewWeaponDataToAdd;
	NewWeaponDataToAdd.WeaponNameInDataTable = WeaponName;

	int32 SlotIndex = Inventory.InventoryData.Add(NewWeaponDataToAdd);

	if (TakeInHands)
	{
		SelectSlotAndApply(SlotIndex);
		ParentActor->OnSwitchWeapon(WeaponName);
	}

	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		FString Wname = el.WeaponNameInDataTable.ToString();
		UE_LOG(LogTemp, Warning, TEXT("----- InvArray %s"), *Wname);
	}

	return true;
}

bool UInventoryComponent::IsWeaponAlreadyExist(FName WeaponName)
{
	bool result = false;

	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		if (el.WeaponNameInDataTable == WeaponName)
			return true;
	}

	return result;
}

bool UInventoryComponent::DeleteWeaponFromInventory(int32 WeaponIndex)
{
	bool Result = true;

	if (!Inventory.InventoryData.IsValidIndex(WeaponIndex))
	{
		return false;
		UE_LOG(LogTemp, Warning, TEXT("Invalid index %i"), WeaponIndex);
	}

	Inventory.InventoryData.RemoveAt(WeaponIndex);

	UE_LOG(LogTemp, Warning, TEXT("--- Index %i removed from inventory"), WeaponIndex);
	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		FString Wname = el.WeaponNameInDataTable.ToString();
		UE_LOG(LogTemp, Warning, TEXT("-- InvArray %s"), *Wname);
	}

	return Result;
}

bool UInventoryComponent::HasFreeSpace()
{
	ClearUnregistredItems();
	return Inventory.InventoryCapacity > Inventory.InventoryData.Num();
}

int32 UInventoryComponent::GetCurrentSelectedSlot()
{
	return Inventory.CurrentSelectedSlot;
}

void UInventoryComponent::SetCurrentSelectedSlot(int32 CurSlot)
{
	Inventory.CurrentSelectedSlot = CurSlot;
}

void UInventoryComponent::ClearUnregistredItems()
{
	UTPSGameInstance* GameInstance = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

	int32 i = 0;
	TArray<int32> TempIdsToRemove;

	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		FName WeaponName = FName(el.WeaponNameInDataTable.ToString());
		FWeaponInfo WeaponInfoResult;
		bool IsFind = true;

		if (GameInstance)
			IsFind = GameInstance->GetWeaponInfoByName(WeaponName,WeaponInfoResult);

		if (!IsFind)
			TempIdsToRemove.Push(i);

		i++;
	}

	if (TempIdsToRemove.Num() <= 0)
		return;

	for (int32 l = TempIdsToRemove.Num() - 1; l >= 0; l--)
	{
		int32 WhereToDelete = TempIdsToRemove[l];
		FString BadName = Inventory.InventoryData[WhereToDelete].WeaponNameInDataTable.ToString();
		Inventory.InventoryData.RemoveAt(WhereToDelete);

		UE_LOG(LogTemp, Warning, TEXT("Bad weapon removed from inventory %s, this class is not exist in Weapon DataTable"), *BadName);
	}
}

bool UInventoryComponent::IsAmmoTypeSlotAlreadyExits(EWeaponType WeaponAmmoType, int32& index)
{
	bool result = false;
	int32 counter = 0;

	for (FAmmoSlot el : Inventory.InventoryAmmoData)
	{
		if (el.WeaponType == WeaponAmmoType)
		{
			result = true;
			//UE_LOG(LogTemp, Warning, TEXT("tp %s"), *el.WeaponTypeFName.ToString());
			index = counter;
		}
		counter++;
	}

	if (!result)
		index = -1;

	return result;
}

bool UInventoryComponent::IsAmmoTypeSlotAlreadyExits(FName WeaponName, int32& index)
{
	bool result = false;
	int32 counter = 0;

	for (FAmmoSlot el : Inventory.InventoryAmmoData)
	{
		if (el.WeaponTypeFName == WeaponName)
		{
			result = true;
			UE_LOG(LogTemp, Warning, TEXT("tp %s"), *el.WeaponTypeFName.ToString());
			index = counter;
		}
		counter++;
	}

	if (!result)
		index = -1;

	return result;
}

int32 UInventoryComponent::FindAmmoSlotIndexByName(FName WeaponName)
{
	int32 index = 0;

	for (FAmmoSlot el : Inventory.InventoryAmmoData)
	{
		if (el.WeaponTypeFName == WeaponName)
			return index;

		index++;
	}

	return -1;
}

int32 UInventoryComponent::FindWeaponSlotIndexByName(FName WeaponName)
{
	int32 index = 0;

	for (FHashCodeInventoryItemData el : Inventory.InventoryData)
	{
		if (el.WeaponNameInDataTable == WeaponName)
			return index;

		index++;
	}

	return -1;
}

void UInventoryComponent::AddAmmoSlot(EWeaponType WeaponAmmoType, FName WeaponName)
{
	int32 SlotIndex = 0;
	if (!IsAmmoTypeSlotAlreadyExits(WeaponAmmoType, SlotIndex))
	{
		FAmmoSlot NewAmmoSlot;
		NewAmmoSlot.WeaponType = WeaponAmmoType;
		NewAmmoSlot.WeaponTypeFName = WeaponName;

		Inventory.InventoryAmmoData.Add(NewAmmoSlot);
	}
}

void UInventoryComponent::AddAmmoSlot(EWeaponType WeaponAmmoType, FName WeaponName, int32 BulletsCount, int32 MaxBulletsCount)
{
	int32 SlotIndex = 0;
	if (!IsAmmoTypeSlotAlreadyExits(WeaponAmmoType, SlotIndex))
	{
		FAmmoSlot NewAmmoSlot;
		NewAmmoSlot.WeaponType = WeaponAmmoType;
		NewAmmoSlot.WeaponTypeFName = WeaponName;
		NewAmmoSlot.AmmoCount = BulletsCount;
		NewAmmoSlot.MaxAmmoCount = MaxBulletsCount;

		Inventory.InventoryAmmoData.Add(NewAmmoSlot);
	}
}

//ToDo
bool UInventoryComponent::AddBulletsToAmmoSlot(EWeaponType WeaponAmmoType, int32 BulletsCount, int32& BulletsOverFlowCount)
{
	UTPSGameInstance* GI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

	FName WeaponName = GetWeaponNameByType(WeaponAmmoType);
	bool result = false;
	int32 WeaponSectionIndex = -1;

	bool bWeaponSectionFound = IsAmmoTypeSlotAlreadyExits(WeaponAmmoType, WeaponSectionIndex);
	if (!bWeaponSectionFound)
	{
		if (GI)
		{
			FWeaponInfo CurWeapInfo;
			GI->GetWeaponInfoByName(WeaponName, CurWeapInfo);
			AddAmmoSlot(WeaponAmmoType, WeaponName, BulletsCount, CurWeapInfo.MaxInventoryRound);
		}
	}
	else
	{
		Inventory.InventoryAmmoData[WeaponSectionIndex].AmmoCount += BulletsCount;
		ATPSCharacter* CastedOwner = Cast<ATPSCharacter>(InventoryOwner);
		if (CastedOwner && CastedOwner->GetCurrentWeaponName() == WeaponName)
		{
			CastedOwner->OnSwitchWeapon(WeaponName);
		}
	}

	//UE_LOG(LogTemp, Warning, TEXT("found %i, index %i"), bWeaponSectionFound, WeaponSectionIndex);

	return false;
}

bool UInventoryComponent::AddBulletsToCurrentAmmoSlot(int32 BulletsCount, int32& BulletsOverFlowCount)
{
	int32 index = -1;
	int32 CurSelWeapSlot = GetCurrentSelectedSlot();

	if (!Inventory.InventoryData.IsValidIndex(CurSelWeapSlot))
		return false;

	FName CurWeapName = Inventory.InventoryData[CurSelWeapSlot].WeaponNameInDataTable;

	//deb
	FString wname = CurWeapName.ToString();

	bool bIsExist = IsAmmoTypeSlotAlreadyExits(CurWeapName, index);
	
	if (!bIsExist)
		return false;

	int32 CurWeaponAmmoSlotIndex = FindAmmoSlotIndexByName(CurWeapName);
	FAmmoSlot& CurWeapSlotStruct = Inventory.InventoryAmmoData[CurWeaponAmmoSlotIndex];

	int32 MinAmmo = 0;
	int32 MaxAmmo = CurWeapSlotStruct.MaxAmmoCount;
	int32 CurAmmo = CurWeapSlotStruct.AmmoCount;
	int32 PotentialAmmoCount = CurAmmo + BulletsCount;

	if (PotentialAmmoCount > MaxAmmo)
	{
		BulletsOverFlowCount = PotentialAmmoCount - MaxAmmo;
		CurWeapSlotStruct.AmmoCount = MaxAmmo;
	} 
	else if (PotentialAmmoCount < MinAmmo)
	{
		BulletsOverFlowCount = MinAmmo + BulletsCount;
		CurWeapSlotStruct.AmmoCount = MinAmmo;
	}
	else if (PotentialAmmoCount >= MinAmmo && PotentialAmmoCount <= MaxAmmo)
	{
		BulletsOverFlowCount = 0;
		CurWeapSlotStruct.AmmoCount = PotentialAmmoCount;
	}

	UE_LOG(LogTemp, Warning, TEXT("WepName %s, PotentialAmmo %i, AllAmmo %i"), *wname, PotentialAmmoCount, CurWeapSlotStruct.AmmoCount);

	ATPSCharacter* CastedOwner = Cast<ATPSCharacter>(InventoryOwner);
	if (CastedOwner)
		CastedOwner->OnSwitchWeapon(CurWeapName);

	return true;
}

int32 UInventoryComponent::GetBulletsCountFromCurrentAmmoSlot()
{
	int32 CurSelWeapSlot = GetCurrentSelectedSlot();

	if (!Inventory.InventoryData.IsValidIndex(CurSelWeapSlot))
		return 0;

	FName CurWeapName = Inventory.InventoryData[CurSelWeapSlot].WeaponNameInDataTable;
	int32 CurWeaponAmmoSlotIndex = FindAmmoSlotIndexByName(CurWeapName);

	if (!Inventory.InventoryAmmoData.IsValidIndex(CurWeaponAmmoSlotIndex))
		return 0;

	return Inventory.InventoryAmmoData[CurWeaponAmmoSlotIndex].AmmoCount;
}

void UInventoryComponent::AddCurrentWeaponAmmo(int32 Bullets)
{
	int32 CurWeapSlot = GetCurrentSelectedSlot();
	FName CurWeapName = "Free";

	if (Inventory.InventoryData.IsValidIndex(CurWeapSlot))
	{
		int32& CurWeapAmmo = Inventory.InventoryData[CurWeapSlot].WeaponLoadedAmmo;
		CurWeapName = Inventory.InventoryData[CurWeapSlot].WeaponNameInDataTable;
		CurWeapAmmo += Bullets;
	}

	ATPSCharacter* ParentActor = Cast<ATPSCharacter>(InventoryOwner);
	ParentActor->OnSwitchWeapon(CurWeapName);
}

FHashCodeWeaponsInventory UInventoryComponent::GetInventoryDataStruct()
{
	return Inventory;
}

bool UInventoryComponent::GetIsWeaponHasBeenInitialized(FName WeaponName)
{
	bool result = false;
	
	int32 index = FindWeaponSlotIndexByName(WeaponName);
	if (index != -1 && Inventory.InventoryData.IsValidIndex(index))
	{
		result = Inventory.InventoryData[index].IsFirstInitialization;
	}

	return result;
}

void UInventoryComponent::SwitchFirstInitializationVarByName(FName WeaponsInventorySlotName, bool bIsFirstInit)
{
	int32 index = FindWeaponSlotIndexByName(WeaponsInventorySlotName);
	if (index != -1 && Inventory.InventoryData.IsValidIndex(index))
	{
		Inventory.InventoryData[index].IsFirstInitialization = bIsFirstInit;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ERROR : UInventoryComponent::SwitchFirstInitializationVarByName - Index not found - %s, %i"), *WeaponsInventorySlotName.ToString(), index);
	}
}

void UInventoryComponent::SwitchFirstInitializationVarByIndex(int32 WeaponsInventorySlotIndex, bool bIsFirstInit)
{
	if (WeaponsInventorySlotIndex != -1 && Inventory.InventoryData.IsValidIndex(WeaponsInventorySlotIndex))
	{
		Inventory.InventoryData[WeaponsInventorySlotIndex].IsFirstInitialization = bIsFirstInit;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ERROR : UInventoryComponent::SwitchFirstInitializationVarByIndex - index not found - %i"), WeaponsInventorySlotIndex);
	}
}

bool UInventoryComponent::SetWeaponAmmoInMagazineByName(FName WeaponName, int32 AmmoCount)
{
	bool result = false;

	int32 WeaponStructIndex = FindWeaponSlotIndexByName(WeaponName);
	if (WeaponStructIndex != -1 && Inventory.InventoryData.IsValidIndex(WeaponStructIndex))
	{
		Inventory.InventoryData[WeaponStructIndex].WeaponLoadedAmmo = AmmoCount;
		result = true;
	}
	else
	{
		result = false;
	}
	return result;
}

int32 UInventoryComponent::GetWeaponAmmoInMagazineByName(FName WeaponName)
{
	int32 result = 0;

	int32 WeaponStructIndex = FindWeaponSlotIndexByName(WeaponName);
	if (WeaponStructIndex != -1 && Inventory.InventoryData.IsValidIndex(WeaponStructIndex))
	{
		result = Inventory.InventoryData[WeaponStructIndex].WeaponLoadedAmmo;
	}
	else
	{
		result = 0;
	}

	return result;
}

FName UInventoryComponent::GetWeaponNameByType(EWeaponType WeaponType)
{
	FName WName = TEXT("Free");

	switch (WeaponType)
	{
	case EWeaponType::TypeFree:
		WName = TEXT("Free");
		break;
	case EWeaponType::TypeRifle:
		WName = TEXT("Rifle");
		break;
	case EWeaponType::TypePistol:
		WName = TEXT("Pistol");
		break;
	case EWeaponType::TypeShotgun:
		WName = TEXT("Shotgun");
		break;
	case EWeaponType::TypeSniperRifle:
		WName = TEXT("SniperRifle");
		break;
	case EWeaponType::TypeGrenadeLauncher:
		WName = TEXT("GrenadeLauncher");
		break;
	default:
		break;
	}

	return WName;
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	InventoryOwner = GetOwner();
	ClearUnregistredItems();

	/*ATPSCharacter* ParentActor = Cast<ATPSCharacter>(InventoryOwner);
	ParentActor->OnSwitchWeapon(FName("Free"));*/
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

