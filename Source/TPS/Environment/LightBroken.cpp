// Fill out your copyright notice in the Description page of Project Settings.


#include "LightBroken.h"
#include "Components/PointLightComponent.h"
#include "Engine/PointLight.h"

// Sets default values
ALightBroken::ALightBroken()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = Frequency;

	//BrokenLight->CreateDefaultSubobject<APointLight>();

	LightComp = CreateDefaultSubobject<UPointLightComponent>(TEXT("BrokenLight"));
	RootComponent = LightComp;
}

// Called when the game starts or when spawned
void ALightBroken::BeginPlay()
{
	Super::BeginPlay();
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = Frequency;

	PointLightComp = Cast<UPointLightComponent>(LightComp);
	PointLightComp->bUseTemperature = bUseLightTemperature;
	PointLightComp->Temperature = LightTemperature;
}

// Called every frame
void ALightBroken::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	PointLightComp = Cast<UPointLightComponent>(LightComp);

	if (PointLightComp)
	{
		FVector Loc = RootComponent->GetComponentLocation();
		FString LocStr = Loc.ToString();

		float RandomLux = FMath::RandRange(LuxMin, LuxMax);

		PointLightComp->UpdateColorAndBrightnessEvent;
		
		float KAVO = PointLightComp->ComputeLightBrightness();
		//PointLightComp->SetLightBrightness(RandomLux);

		PointLightComp->UpdateColorAndBrightness();
	}
}

