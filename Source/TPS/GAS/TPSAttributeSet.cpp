// Fill out your copyright notice in the Description page of Project Settings.

#include "TPS/GAS/TPSAttributeSet.h"

UTPSAttributeSet::UTPSAttributeSet()
{

}

void UTPSAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME_CONDITION_NOTIFY(UTPSAttributeSet, Health, COND_None, REPNOTIFY_Always)
	DOREPLIFETIME_CONDITION_NOTIFY(UTPSAttributeSet, Stamina, COND_None, REPNOTIFY_Always)
	DOREPLIFETIME_CONDITION_NOTIFY(UTPSAttributeSet, Shield, COND_None, REPNOTIFY_Always)
}

bool UTPSAttributeSet::SetComponentOwner(AActor* NewOwner)
{


	return true; //ToDo check
}

void UTPSAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UTPSAttributeSet, Health, OldHealth);
}

void UTPSAttributeSet::OnRep_Stamina(const FGameplayAttributeData& OldStamina)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UTPSAttributeSet, Stamina, OldStamina);
}

void UTPSAttributeSet::OnRep_Shield(const FGameplayAttributeData& OldShield)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UTPSAttributeSet, Shield, OldShield);
}
