// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/MovementComponent.h"
#include "TPSMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UTPSMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
	
};
