// Fill out your copyright notice in the Description page of Project Settings.


#include "EjectThingsBase.h"

// Sets default values
AEjectThingsBase::AEjectThingsBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = StaticMesh;
	StaticMesh->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AEjectThingsBase::BeginPlay()
{
	Super::BeginPlay();

	FVector SM_Vector = StaticMesh->GetSocketLocation("AmmoEject");
	FVector SM_ForwardVector = StaticMesh->GetForwardVector();

	FVector RotatedVector = SM_ForwardVector.RotateAngleAxis(90, FVector(0, 0, 1));

	StaticMesh->AddForce(RotatedVector *5000);
	//CapsuleComponent->AddForce(FVector(100000, 0, 5));

	
}

// Called every frame
void AEjectThingsBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

