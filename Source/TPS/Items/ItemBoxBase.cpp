// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemBoxBase.h"
#include "TPS/Character/TPSCharacter.h"
#include "TPS/Components/InventoryComponent.h"

// Sets default values
AItemBoxBase::AItemBoxBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	BoxMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshBox"));
	BoxMesh->SetupAttachment(RootComponent);
	BoxMesh->SetSimulatePhysics(true);
	BoxMesh->SetCollisionProfileName(TEXT("Destroyable"));

	LightSpotter = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
	LightSpotter->AttachTo(BoxMesh);

	Decal = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	Decal->SetupAttachment(BoxMesh);

	PlaneWithAmmoPreviewTexture = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AmmoPreview"));
	PlaneWithAmmoPreviewTexture->SetupAttachment(BoxMesh);
}

void AItemBoxBase::SetDefaultDecalColor()
{
	DynMat->SetVectorParameterValue(FName("Color"), FLinearColor(1, 0.83, 0, 1));
	CurrentDecalColor = EBoxColor::Default;
	TogglePreviewMeshVisibility(false);
}

void AItemBoxBase::SetActiveDecalColor()
{
	DynMat->SetVectorParameterValue(FName("Color"), FLinearColor(1, 1, 1, 1));
	CurrentDecalColor = EBoxColor::Active;
	TogglePreviewMeshVisibility(true);
}

void AItemBoxBase::TogglePreviewMeshVisibility(bool bVisible)
{
	if (PlaneWithAmmoPreviewTexture)
	{
		PlaneWithAmmoPreviewTexture->SetVisibility(bVisible);
	}
}

FVector AItemBoxBase::GetAmmoBoxWorldLocation()
{
	if (BoxMesh)
	{
		return BoxMesh->GetComponentToWorld().GetLocation();
	}
	else
	{
		return FVector::ZeroVector;
	}
}

EBoxColor AItemBoxBase::GetCurrentEnumColor()
{
	return CurrentDecalColor;
}

// Called when the game starts or when spawned
void AItemBoxBase::BeginPlay()
{
	Super::BeginPlay();
	
	x = radius * sin(alpha) - centrX;
	DynamicMatInterface = Decal->GetMaterial(0);

	DynMat = UMaterialInstanceDynamic::Create(DynamicMatInterface, DynMat);

	Decal->SetDecalMaterial(DynMat);
	PlaneWithAmmoPreviewTexture->SetCollisionProfileName("NoCollision");
	TogglePreviewMeshVisibility(false);
	
}

// Called every frame
// A made this to highlight ammoboxes and interactable objects
void AItemBoxBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (alpha > 360)
	{
		alpha = 0;
	}
	
	if (PlaneWithAmmoPreviewTexture)
	{
		PlaneWithAmmoPreviewTexture->AddLocalRotation(FQuat(FRotator(0,1,0)), false);
	}

	float TempAlpha = alpha * DeltaTime;

	x = radius * sin(TempAlpha);
	//y = sin(90 - alpha) * radius;

	alpha = alpha + 1;

	LightSpotter->SetIntensity(x * 1000);
	DynMat->SetScalarParameterValue(FName("Outer density"), x * 10);

}

EInteractionType AItemBoxBase::GetInteractionType()
{
	return EInteractionType::Pickup;
}

bool AItemBoxBase::ExecuteInteract(ATPSCharacter* ActionInstigator)
{
	bool result = false;
	int32 overflowedbullets;
	FName WeaponName("None");

	switch (AmmoFor)
	{
	case EWeaponType::TypeFree:
		break;
	case EWeaponType::TypeRifle:
		WeaponName = FName("Rifle");
		break;
	case EWeaponType::TypePistol:
		WeaponName = FName("Pistol");
		break;
	case EWeaponType::TypeShotgun:
		WeaponName = FName("Shotgun");
		break;
	case EWeaponType::TypeSniperRifle:
		WeaponName = FName("SniperRifle");
		break;
	case EWeaponType::TypeGrenadeLauncher:
		WeaponName = FName("GrenadeLauncher");
		break;
	default:
		break;
	}

	UInventoryComponent* InstigatorsInventory = ActionInstigator->GetInventoryComponent();
	if (InstigatorsInventory)
	{
		bool bIsWeaponExist = InstigatorsInventory->IsWeaponAlreadyExist(WeaponName);
		if (bIsWeaponExist)
		{
			result = InstigatorsInventory->AddBulletsToAmmoSlot(AmmoFor, AmmoCount, overflowedbullets);
		}
		else
		{
			result = InstigatorsInventory->AddWeaponToInventory(WeaponName,true);
		}
	}

	Destroy(true);
	return result;
}

